# Smallstep

https://smallstep.com/docs/step-ca/basic-certificate-authority-operations

# What is it?

Smallstep is a most full featured PKI solution which may replace my EJBCA installation.

# Development

In a first step this repository should provide scripts for maintaining an PKI.

In a second step it should provides script for automated migrations of EJBCA
community edition to Smallstep PKI.

# Reasons to replace EJBCA

1. Primekey was purchased by an US based company, so it does no longer fulfill european
data protection standards. This is caused by US laws.

2. Important features like ACME are not included in free edition of EJBCA. Smallstep
supports ACME.

3. EJBCA community edition seems no longer (or maybe very slow) developed.

4. EJBCA may still a good solution for large enterprise PKI, but for private use
there are better solutions.

